'use client';
import React from 'react';
import clsx from 'clsx';
import { Play, Pause, RotateCcw } from 'react-feather';
import { motion } from 'framer-motion';

import Card from '@/components/Card';
import VisuallyHidden from '@/components/VisuallyHidden';

import styles from './CircularColorsDemo.module.css';

const COLORS = [
  { label: 'red', value: 'hsl(348deg 100% 60%)' },
  { label: 'yellow', value: 'hsl(50deg 100% 55%)' },
  { label: 'blue', value: 'hsl(235deg 100% 65%)' }
];

function CircularColorsDemo() {
  const [timeElapsed, setTimeElapsed] = React.useState(0);
  const [loopIsActive, setLoopIsActive] = React.useState(false);
  const layoutId = React.useId();
  const selectedColor = COLORS[timeElapsed % COLORS.length];

  React.useEffect(() => {
    if (!loopIsActive) {
      return;
    }
    const timer = setInterval(() => {
      setTimeElapsed((currentTimer) => currentTimer + 1);
    }, 1000);

    return () => {
      if (loopIsActive && timer) {
        clearInterval(timer);
      }
    };
  }, [timeElapsed, loopIsActive]);

  function handleLoopControl() {
    setLoopIsActive(!loopIsActive);

    // we want to go to the next step immediately on play
    // for a better understanding of the demo. No need to have a full second or a real timer at that moment
    if (!loopIsActive) {
      setTimeElapsed(timeElapsed + 1);
    }
  }

  function handleReset() {
    setTimeElapsed(0);
    setLoopIsActive(false);
  }

  return (
    <Card
      as="section"
      className={styles.wrapper}>
      <ul className={styles.colorsWrapper}>
        {COLORS.map((color, index) => {
          const isSelected = color.value === selectedColor.value;

          return (
            <li
              className={styles.color}
              key={index}>
              {isSelected && (
                <motion.div
                  layoutId={`${layoutId}-selected-item`}
                  className={styles.selectedColorOutline}
                />
              )}
              <div
                className={clsx(styles.colorBox, isSelected && styles.selectedColorBox)}
                style={{
                  backgroundColor: color.value
                }}>
                <VisuallyHidden>{color.label}</VisuallyHidden>
              </div>
            </li>
          );
        })}
      </ul>

      <div className={styles.timeWrapper}>
        <dl className={styles.timeDisplay}>
          <dt>Time Elapsed</dt>
          <dd>{timeElapsed}</dd>
        </dl>
        <div className={styles.actions}>
          <button onClick={handleLoopControl}>
            {loopIsActive ? (
              <>
                <Pause />
                <VisuallyHidden>Pause</VisuallyHidden>
              </>
            ) : (
              <>
                <Play />
                <VisuallyHidden>Play</VisuallyHidden>
              </>
            )}
          </button>
          <button>
            <RotateCcw onClick={handleReset} />
            <VisuallyHidden>Reset</VisuallyHidden>
          </button>
        </div>
      </div>
    </Card>
  );
}

export default CircularColorsDemo;
