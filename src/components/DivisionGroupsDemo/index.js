// Lazy load component here to facilitate implementation in blog posts
// Here we wrap up our export with the lazy loading wrapper
import dynamic from 'next/dynamic';
const DivisionGroupsDemo = dynamic(() => import('./DivisionGroupsDemo'));

export default DivisionGroupsDemo;
