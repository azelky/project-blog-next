import React from 'react';
import Link from 'next/link';
import { format } from 'date-fns';

import Card from '@/components/Card';

import styles from './BlogSummaryCard.module.css';

function BlogSummaryCard({ slug, title, publishedOn, abstract }) {
  const href = `/${slug}`;
  const humanizedDate = format(new Date(publishedOn), 'MMMM do, yyyy');

  return (
    <Card className={styles.wrapper}>
      <Link
        href={href}
        className={styles.title}
        prefetch={true}>
        {/* use prefetch with care because it can eat up a lot of bandwidth and cost money if it's on too many articles. A solution can be to use prefetch only on 5 first articles for example*/}
        {title}
      </Link>
      <time dateTime={publishedOn}>{humanizedDate}</time>
      <p>
        {abstract}{' '}
        <Link
          href={href}
          className={styles.continueReadingLink}>
          Continue reading <span className={styles.arrow}>→</span>
        </Link>
      </p>
    </Card>
  );
}

export default BlogSummaryCard;
