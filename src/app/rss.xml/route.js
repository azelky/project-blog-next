import { BLOG_TITLE, BLOG_DESCRIPTION } from '@/constants';
import { getBlogPostList } from '@/helpers/file-helpers';
import RSS from 'rss';

export async function GET() {
  // Feed general options
  const feedOptions = {
    title: BLOG_TITLE,
    description: BLOG_DESCRIPTION
  };
  // We create a RSS feed
  const feed = new RSS(feedOptions);

  // Get all posts
  const posts = await getBlogPostList();

  // Create a new feed item for each post
  posts.forEach(({ slug, title, abstract, publishedOn }) => {
    feed.item({
      title,
      description: abstract,
      date: publishedOn,
      url: `https://www.demo.com/${slug}`
    });
  });

  // Generate the raw XML string using `feed.xml`
  const xml = feed.xml({ indent: true });

  // Send it to the client. We need to set the Content-Type
  // header so that browsers / RSS clients will interpret
  // it as XML, and not as plaintext
  return new Response(xml, {
    headers: { 'Content-Type': `application/xml` }
  });
}
