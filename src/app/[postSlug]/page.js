import React from 'react';
import { notFound } from 'next/navigation';

import { MDXRemote } from 'next-mdx-remote/rsc';

import BlogHero from '@/components/BlogHero';
import COMPONENT_MAP from '@/helpers/mdx-components';

import { loadBlogPost } from '@/helpers/file-helpers';
import { BLOG_TITLE } from '@/constants';

import styles from './postSlug.module.css';

export async function generateMetadata({ params }) {
  const blogPostsData = await loadBlogPost(params.postSlug);

  if (!blogPostsData) {
    return null;
  }
  const { frontmatter } = blogPostsData;

  return {
    title: `${frontmatter.title} • ${BLOG_TITLE}`,
    description: `${frontmatter.abstract}`
  };
}

async function BlogPost({ params }) {
  const post = await loadBlogPost(params.postSlug);

  if (!post) {
    notFound();
  }

  return (
    <article className={styles.wrapper}>
      <BlogHero
        title={post.frontmatter.title}
        publishedOn={post.frontmatter.publishedOn}
      />
      <div className={styles.page}>
        <MDXRemote
          source={post.content}
          components={COMPONENT_MAP}
        />
        <p>This is where the blog post will go!</p>
        <p>
          You will need to use <em>MDX</em> to render all of the elements created from the blog post in this spot.
        </p>
      </div>
    </article>
  );
}

export default BlogPost;
