import Link from 'next/link';

export default function NotFound() {
  return (
    <article
      style={{
        maxWidth: 'var(--content-width)',
        margin: '0 auto',
        padding: '64px var(--viewport-padding) 32px',
        textAlign: 'center'
      }}>
      <div>
        <h2>404 - Page Not Found</h2>
        <p>Could not find requested resource</p>
        <Link href="/">Return Home</Link>
      </div>
    </article>
  );
}
